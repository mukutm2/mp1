/* Your JS here. */

let currentSlideIndex = 0;

function showSlide(index) {
    const slides = document.querySelectorAll('.slide');
    if (index >= slides.length) currentSlideIndex = 0;
    if (index < 0) currentSlideIndex = slides.length - 1;

    // Hide all slides
    slides.forEach(slide => {
        slide.style.display = 'none';
    });

    // Show the current slide
    slides[currentSlideIndex].style.display = 'block';
}

function moveSlide(direction) {
    showSlide(currentSlideIndex += direction);
}

// Initialize the carousel on page load
document.addEventListener('DOMContentLoaded', () => {
    showSlide(currentSlideIndex);
});

// Navbar Resizing on Scroll
window.addEventListener('scroll', function() {
    const header = document.querySelector('.header-content');
    if (window.pageYOffset > 50) { // Adjust the number as needed
        header.classList.add('small-header');
    } else {
        header.classList.remove('small-header');
    }
});

// Function to highlight the active section in the navbar
function highlightActiveSection() {
    const sections = document.querySelectorAll('section');
    const navLinks = document.querySelectorAll('#navbar ul li a');

    let passedSections = Array.from(sections).map((section, index) => {
        return {
            y: section.getBoundingClientRect().top - navbar.offsetHeight,
            id: index
        };
    }).filter(section => section.y <= 0);

    let currentSectionId = passedSections.at(-1)?.id || 0;

    navLinks.forEach(link => link.classList.remove('active'));
    navLinks[currentSectionId].classList.add('active');
}

// Event listener for scroll
window.addEventListener('scroll', highlightActiveSection);

document.addEventListener('DOMContentLoaded', () => {
    const albumItems = document.querySelectorAll('.album-item');

    albumItems.forEach(item => {
        item.addEventListener('click', () => {
            const details = item.querySelector('.album-details');
            const isOpen = details.style.maxHeight;
            details.style.maxHeight = isOpen ? null : `${details.scrollHeight}px`; // Assign the max height for expand/collapse
            details.style.opacity = isOpen ? 0 : 1; // Fade in/out the details
        });
    });
});

function openModal(modalId) {
    const modal = document.getElementById(modalId);
    modal.style.display = 'block';
    document.body.style.overflow = 'hidden'; // Prevent scrolling of the background
}

function closeModal(modalId) {
    const modal = document.getElementById(modalId);
    modal.style.display = 'none';
    document.body.style.overflow = ''; // Allow scrolling again
}

// Optional: close modal if the user clicks outside of it
window.onclick = function(event) {
    if (event.target.classList.contains('modal')) {
        event.target.style.display = 'none';
        document.body.style.overflow = ''; // Allow scrolling again
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const observer = new IntersectionObserver((entries) => {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                entry.target.classList.add('fade-in-section');
            }
        });
    }, {
        threshold: 0.1 // Trigger when at least 10% of the target is visible
    });

    const sections = document.querySelectorAll('section');
    sections.forEach(section => {
        observer.observe(section);
    });
});
